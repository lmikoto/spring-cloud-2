package com.lagou.dao;

import com.lagou.model.AuthCode;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 验证码DAO
 */
public interface AuthCodeDao extends JpaRepository<AuthCode, Integer> {
}

package com.lagou.model;

import javax.annotation.Generated;
import javax.persistence.*;
import java.util.Date;

/**
 * 验证码
 */
@Entity
@Table(name = "lagou_auth_code")
public class AuthCode {
    /**
     * 验证码ID
     */
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    /**
     * 邮箱地址
     */
    private String email;

    /**
     * 验证码
     */
    private String code;

    /**
     * 创建时间
     */
    @Column(name = "createtime")
    private Date createTime;

    /**
     * 过期时间
     */
    @Column(name = "expiretime")
    private Date expireTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    @Override
    public String toString() {
        return "AuthCode{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", code='" + code + '\'' +
                ", createTime=" + createTime +
                ", expireTime=" + expireTime +
                '}';
    }
}

package com.lagou;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableDubbo
public class LagouServiceCodeApplication8081 {

    public static void main(String[] args) {
        SpringApplication.run(LagouServiceCodeApplication8081.class, args);
    }

}

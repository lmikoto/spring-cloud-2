package com.lagou.controller;

import com.lagou.service.AuthCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/code")
public class AuthCodeController {
    /**
     * 验证码服务
     */
    @Autowired
    private AuthCodeService authCodeService;

    /**
     * 发送验证码到指定邮箱
     *
     * @param email 指定的邮箱
     * @return 操作结果
     */
    @GetMapping("/create/{email}")
    public boolean sendCode(@PathVariable("email") String email) {
        return authCodeService.sendCode(email);
    }

    /**
     * 校验验证码
     *
     * @param email 验证码对应的邮箱
     * @param code 验证码
     * @return 校验结果
     */
    @PostMapping("/validate/{email}/{code}")
    public int validateCode(@PathVariable("email") String email, @PathVariable("code") String code) {
        return authCodeService.validateCode(email, code);
    }
}

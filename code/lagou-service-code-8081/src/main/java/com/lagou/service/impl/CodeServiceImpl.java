package com.lagou.service.impl;

import com.lagou.service.AuthCodeService;
import com.lagou.service.CodeService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 验证码服务
 */
@Service
public class CodeServiceImpl implements CodeService {
    /**
     * 验证码认证服务
     */
    @Autowired
    private AuthCodeService authCodeService;

    /**
     * 校验验证码
     *
     * @param email 邮箱地址
     * @param code 验证码
     * @return 校验结果
     */
    @Override
    public int validateCode(String email, String code) {
        return authCodeService.validateCode(email, code);
    }
}

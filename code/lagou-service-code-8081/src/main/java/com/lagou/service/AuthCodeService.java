package com.lagou.service;

/**
 * 验证码服务
 */
public interface AuthCodeService {
    /**
     * 发送验证码到指定的邮箱
     *
     * @param email 指定的邮箱
     * @return 操作结果
     */
    boolean sendCode(String email);

    /**
     * 校验验证码
     *
     * @param email 验证码对应的邮箱
     * @param code 验证码
     * @return 校验结果
     */
    int validateCode(String email, String code);
}

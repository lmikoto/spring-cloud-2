package com.lagou.service.impl;

import com.lagou.service.EmailService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 * 邮件服务
 */
@Service
public class EmailServiceImpl implements EmailService {
    /**
     * 邮件发送器
     */
    @Autowired
    private JavaMailSender mailSender;

    /**
     * 发送邮件
     *
     * @param email 邮箱地址
     * @param code 验证码
     * @return 操作结果
     */
    @Override
    public boolean sendEmail(String email, String code) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("lmikoto9@126.com");

        mailMessage.setTo(email);
        mailMessage.setSubject("验证码");

        mailMessage.setText(code);

        try {
            mailSender.send(mailMessage);
        } catch (Exception ex) {
            System.out.println(String.format("Failed to send message to %s", email));
            ex.printStackTrace();

            return false;
        }

        return true;
    }
}

package com.lagou.service;

/**
 * @author liuyang
 * 2021/5/24 8:43 下午
 */
public interface EmailService {

     boolean sendEmail(String email, String code);
}

package com.lagou.service;

/**
 * @author liuyang
 * 2021/5/24 8:41 下午
 */
public interface CodeService {

    int validateCode(String code,String name);
}

package com.lagou.model;

import javax.persistence.*;

/**
 * Token信息
 */
@Entity
@Table(name = "lagou_token")
public class Token {
    /**
     * Token的ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Token对应的邮箱
     */
    private String email;

    /**
     * Token值
     */
    private String token;

    @Override
    public String toString() {
        return "Token{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

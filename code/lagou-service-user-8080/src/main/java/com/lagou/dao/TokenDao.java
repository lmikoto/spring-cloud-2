package com.lagou.dao;

import com.lagou.model.Token;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * TokenDAO
 */
public interface TokenDao extends JpaRepository<Token, Integer> {
}

package com.lagou.dao;

import com.lagou.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 用户DAO
 */
public interface UserDao extends JpaRepository<User, Integer> {
}

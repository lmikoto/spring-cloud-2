package com.lagou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SpringCloudGatewayApplication9002 {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudGatewayApplication9002.class, args);
    }

}

package com.lagou.filter;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 访问记录
 */
public class AccessInfo {
    /**
     * 开始访问的时间
     */
    private volatile long startTime;

    /**
     * 访问次数
     */
    private AtomicInteger accessTimes;

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public AtomicInteger getAccessTimes() {
        return accessTimes;
    }

    public void setAccessTimes(AtomicInteger accessTimes) {
        this.accessTimes = accessTimes;
    }
}
